import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;
Vue.prototype.$appName = "Learnbread";
Vue.prototype.$slidercounter = null;
Vue.prototype.$testimonyslidercounter = null;
Vue.prototype.$loadingText = "Please wait... If this persist, check your network connection";
Vue.prototype.$appCurrency = "N";
//Vue.prototype.$http = axios;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
