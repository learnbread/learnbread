import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Auth from "./services/middleware/auth.js";

Vue.use(Router);

export default new Router({
	mode: 'history',
	routes: [
		{ 
			path: '*', 
			component: () => 
			import(/* webpackChunkName: "posts" */ "./views/NotFound.vue") 
		},
		{
			path: "/",
			name: "home",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: Home
		},
		{
			path: "/user/getstarted",
			name: "getstarted",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Guest(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/GetStarted.vue")
		},
		{
			path: "/user/myactivated",
			name: "myactivated",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Guest(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/MyActivated.vue")
		},
		{
			path: "/createnewpassword/:tk",
			name: "createnewpassword",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/CreateNewPassword.vue")
		},
		
		{
			path: "/expiredlink",
			name: "expiredlink",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "about" */ "./views/ExpiredLink.vue")
		},
		{
			path: "/policy",
			name: "policy",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/Policy.vue")
		},
		{
			path: "/marketplace",
			name: "marketplace",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.User(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/MarketPlace.vue")
		},
		{
			path: "/search",
			name: "search",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.User(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/Search.vue")
		},
		{
			path: "/ourcommunity",
			name: "ourcommunity",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/OurCommunity.vue")
		},
		{
			path: "/donate",
			name: "donate",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/Donate.vue")
		},
		{
			path: "/marketplace/buybread",
			name: "buybread",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.User(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/BuyBread.vue")
		},
		{
			path: "/marketplace/scoutbread",
			name: "scoutbread",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.User(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/ScoutBread.vue")
		},
		{
			path: "/marketplace/sellbread",
			name: "sellbread",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.User(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/SellBread.vue")
		},
		{
			path: "/marketplace/sendbread",
			name: "sendbread",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.User(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/SendBread.vue")
		},
		{
			path: "/:username",
			name: "profile",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/Profile.vue")
		},
		{
			path: "/:username/editprofile",
			name: "editprofile",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.User(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/EditProfile.vue")
		},
		{
			path: "/:username/timeline",
			name: "timeline",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.User(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/Timeline.vue")
		},
		{
			path: "/:username/categories",
			name: "usercategories",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.User(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/Categories.vue")
		},
		{
			path: "/:username/:category",
			name: "learningzone",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.User(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/LearningZone.vue")
		},
		{
			path: "/:username/:cat_slug/notes",
			name: "notes",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.User(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import(/* webpackChunkName: "posts" */ "./views/User/Notes.vue")
		},
		
		/* {
			path: "/resetaccess",
			name: "resetaccess",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Guest(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import("./views/ResetAccess.vue")
		}, */
		
		/* {
			path: "/posts",
			name: "posts",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () =>
			import("./views/Posts.vue")
		}, */
		{
			path: "/:category",
			name: "category",
			component: () => import("./views/Category.vue"),
			children: [
				{
					// UserPosts will be rendered inside User's <router-view>
					// when /user/:id/posts is matched
					path: ':slug',
					name: 'post',
					component: () =>
					import(/* webpackChunkName: "posts" */ "./views/Post.vue")
				}
			],
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			}
		},
		{
			path: "/page/:slug",
			name: "page",
			beforeEnter: (to, from, next) => {
				let options = {to, from, next};
				Auth.Everyone(options);
			},
			meta:{
				middleware: true
			},
			component: () => import("./views/Page.vue")
		}
	]
});
