//import axios from 'axios';
import LocalResponse from '@/json/localJson/LocalResponse.json';

export default{
    name: "ApiRequest",
    LocalResponse,
    defaultURL: 'https://lbipa.learnbread.com/',
    //defaultURL: 'http://127.0.0.1:5000/',
    w_platform: navigator.platform,

    getAboutPage(){
        //return console.log(axios.post());
    },

    getUtoken(){
        const pdata = JSON.parse(localStorage.getItem('process_data'));
        if(pdata){
            return pdata.utfa
        }else{
            return "0"
        }
    },

    convertToFormData(ddata){
        let formData = new FormData();	
								
        for ( var key in ddata) {
            formData.append(key, ddata[key]);
        }

        return formData;
    },

    getUserData(){
        const pdata = JSON.parse(localStorage.getItem('process_data'));
        const nouser = {};
        nouser["id"] = 0
        if(pdata){
            if(pdata.data){
                return pdata.data.user
            }else{
                return nouser;
            }
        }else{
            return nouser;
        }
    },
    onlineStatus(){
        var os_elem = document.getElementById("onlinestatus");
        var online_elem = document.getElementById("onlinestat");
        var offline_elem = document.getElementById("offlinestat");

        if(window.navigator.onLine){
            return true;
        }else{
            if (os_elem.style.display === "none") {
                os_elem.style.display = "block";
                online_elem.style.display = "none";
                offline_elem.style.display = "block";
            }else{
                os_elem.style.display = "block";
                online_elem.style.display = "none";
                offline_elem.style.display = "block";
            }
            return false;
        }
    },

    post(data){
        data['platform'] = this.w_platform;
        data['userdata'] = JSON.stringify(this.getUserData());
        return this.proceed2(data); 
    },

    get(i_data){
        i_data['platform'] = this.w_platform;
        i_data['userdata'] = JSON.stringify(this.getUserData());
        
        return this.proceed2(i_data);
    },
    async postData(data) {
        // Default options are marked with *
        let auth = this.getUtoken();
        let url = this.defaultURL + data.string
        const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
            'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(this.convertToFormData(data)) // body data type must match "Content-Type" header
        });
        return response.json(); // parses JSON response into native JavaScript objects
    },
    proceed2(data){
        let send_data = this.convertToFormData(data);
        let URL = this.defaultURL + data.string;
        //console.log(apiurl);
        let options = {
            method: 'POST', // or 'PUT'
            headers: {
                //'Content-Type': 'multipart/form-data',
                'Accept': 'application/json',
                'Authorization': this.getUtoken()
            },
            body: send_data,
        };
        return fetch(URL, options)
        .then(response => {
            if (response.ok) {
                const contentType = response.headers.get('content-type');
                if (contentType || contentType.includes('application/json')) {
                    return response.json();
                }else{
                    throw new TypeError("Oops, we haven't got JSON!");
                }
            }else{
                let response_v = Promise.resolve(this.LocalResponse.error.requesterror);
                return response_v;
            }
        })
        .catch((error) => {
            //console.error('Error:', error);
            let response_v = Promise.resolve(this.LocalResponse.error.requesterror);
            return response_v;
        });
    },
    proceed(i_data){
        if(this.onlineStatus()){
            
            /* return axios({
                method: 'post',
                url:i_data.string,
                baseURL: this.defaultURL,
                data: this.convertToFormData(i_data),
                headers: {
                    'Content-Type': 'multipart/form-data',
                    'Accept': 'application/json',
                    "Access-Control-Allow-Origin": "*",
                    "Access-Control-Allow-Headers": "*",
                    Authorization: this.getUtoken()
                }
            })
            .then( response => {
                console.log("Yes we are here", response);
                return response.data;
            })
            .catch(function (error) {
                console.log(error);
            }); */
        }else{
            let response_v = Promise.resolve(this.LocalResponse.error.onlinestatus);
            return response_v;
        }
    }
};