import successJson from "@/json/localJson/LocalResponse.json";

export default{
    name: "LocalApi",
    successJson,

    post(data){
        let string = data['string'];
        if(string == "front/signup.php"){
            let response = Promise.resolve(this.successJson.guest.signup);
            //console.log("yes we are ready to signup", response);
            return response;
        }
        if(string == "userlogin"){
            let response = Promise.resolve(this.successJson.success.login);
            return response;
        }
    },

    postimg(string){
        if(string == "user/createbook.php"){
            let response = Promise.resolve(this.successJson.reader.createbook);
            return response;
        }
    },
    get(data){
        if(data.string == "view_getmenus"){
            return Promise.resolve(this.successJson.success.menu.guest);
        }

        if(data.string == "recentposts"){
            return Promise.resolve(this.successJson.success.recentposts);
        }

        if(data.string == "gettestimonies"){
            return Promise.resolve(this.successJson.success.gettestimonies);
        }

        if(data.string == "getpartners"){
            return Promise.resolve(this.successJson.success.getpartners);
        }
        if(data.string == "getpagedata"){
            return Promise.resolve(this.successJson.success.pagedata);
        }

        if(data.string == "getcategorydata"){
            return Promise.resolve(this.successJson.success.categorydata);
        }

        if(data.string == "getpostsdata"){
            return Promise.resolve(this.successJson.success.posts);
        }

        if(data.string == "getpostdata"){
            return Promise.resolve(this.successJson.success.post);
        }

        if(data.string == "getuserprofile"){
            return Promise.resolve(this.successJson.success.getuserprofile);
        }
        
        if(data.string == "getusercategories"){
            return Promise.resolve(this.successJson.success.getusercategories);
        }
        if(data.string == "getcategorynotes"){
            return Promise.resolve(this.successJson.success.getcategorynotes);
        }
        if(data.string == "getuserchannels"){
            return Promise.resolve(this.successJson.success.getuserchannels);
        }
        if(data.string == "seemoregetuserchannels"){
            return Promise.resolve(this.successJson.success.seemoregetuserchannels);
        }
        if(data.string == "getsuggestedchannels"){
            return Promise.resolve(this.successJson.success.getsuggestedchannels);
        }
        if(data.string == "seemoregetsuggestedchannels"){
            return Promise.resolve(this.successJson.success.seemoregetsuggestedchannels);
        }
        if(data.string == "getlearningzonecatdata"){
            return Promise.resolve(this.successJson.success.getlearningzonecatdata);
        }

    }
}