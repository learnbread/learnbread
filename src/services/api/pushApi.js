import axios from 'axios'

export default{
    name: "PushApi",
    defaultURL: 'http://127.0.0.1:5000/',

    sendtoAxois(url, data){
        return axios({
            method: 'post',
            url:url,
            //headers: { 'content-type': 'application/json' },
            baseURL: this.defaultURL,
            data: data
        })
        .then(response=>{
            //console.log("this is your response", response.data);
            return response.data;
        });
    }
};