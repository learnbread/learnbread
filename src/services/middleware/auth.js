import MainBase from "@/services/controller/MainBase.js";

export default  {

    name: "Auth",
    Admin(options){
        this.OnProgressBar();
        if(MainBase.guestUser()){
            this.OffProgressBar();
            return options.next(options.from.fullPath);
        }

        if(options.to.meta.hasOwnProperty('middleware')){
            if(options.to.meta.middleware){
                if(MainBase.adminLoggedIn()){
                    this.OffProgressBar();
                    return options.next();    
                }else{
                    this.OffProgressBar();
                    return options.next(options.from.fullPath);
                }
            }else{
                return options.next(options.from.fullPath);
            }
        }
    },

    User(options){
        this.OnProgressBar();
        if(MainBase.guestUser()){
            this.OffProgressBar();
            return options.next(options.from.fullPath);
        }

        if(options.to.meta.hasOwnProperty('middleware')){
            if(options.to.meta.middleware){
                if(MainBase.userLoggedIn()){
                    this.OffProgressBar();
                    return options.next();    
                }else{
                    this.OffProgressBar();
                    return options.next(options.from.fullPath);
                }
            }else{
                return options.next(options.from.fullPath);
            }
        }
    },

    GeneralUsers(options){
        this.OnProgressBar();
        if(options.to.meta.hasOwnProperty('middleware')){
            if(options.to.meta.middleware){
                if(MainBase.userLoggedIn() || MainBase.adminLoggedIn()){
                    this.OffProgressBar();
                    return options.next();   
                }else{
                    this.OffProgressBar();
                    return options.next(options.from.fullPath);
                }
            }else{
                return options.next(options.from.fullPath);
            }
        }
    },

    Guest(options){
        this.OnProgressBar();
        if(options.to.meta.hasOwnProperty('middleware')){
            if(options.to.meta.middleware){
                if(MainBase.guestUser()){
                    this.OffProgressBar();
                    return options.next();   
                }else{
                    this.OffProgressBar();
                    console.log("we are supposed to go to guest page ohh");
                    return options.next(options.from.fullPath);
                }
            }else{
                return options.next(options.from.fullPath);
            }
        }
    },

    Everyone(options){
        this.OnProgressBar();
        if(options.to.meta.hasOwnProperty('middleware')){
            if(options.to.meta.middleware){
                this.OffProgressBar();
                return options.next();
            }else{
                return options.next(options.from.fullPath);
            }
        }
    },

    User(options){
        if(MainBase.guestUser()){
            return options.next(options.from.fullPath);
        }

        if(options.to.meta.hasOwnProperty('middleware')){
            if(options.to.meta.middleware){
                if(MainBase.userLoggedIn()){
                    return options.next();    
                }else{
                    return options.next(options.from.fullPath);
                }
            }else{
                return options.next(options.from.fullPath);
            }
        }
    },
    OnProgressBar(){
        MainBase.showProgressBar();
        //alert("i am on, progress bar");
    },

    OffProgressBar(){
        MainBase.removeProgressBar();
        //alert("i am on, progress bar");
    }
}