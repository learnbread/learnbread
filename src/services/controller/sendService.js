//import ApiRequest from "@/services/api/localApi.js";
import ApiRequest from "@/services/api/api.js";

export default {
    
    name: "SendService",
    ApiRequest,

    post(data){
        return ApiRequest.post(data);
    },

    get(data){
        return ApiRequest.get(data);
    }
};