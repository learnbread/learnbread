import SendService from "@/services/controller/sendService.js";

var os_elem = document.getElementById("onlinestatus");
var online_elem = document.getElementById("onlinestat");
var offline_elem = document.getElementById("offlinestat");
os_elem.style.display = "none";

function removeDiv(){
    setTimeout(function(){
        os_elem.style.display = "none";
    }, 5000);
}
function checkcompact(){
    alert(".....");
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.platform) ) {
                
    }else{
        window.location.href = "https://desktop.learnbread.com";
    }
}
//checkcompact();
window.addEventListener('online', () => {
    if (os_elem.style.display === "none") {
        os_elem.style.display = "block";
        online_elem.style.display = "block";
        offline_elem.style.display = "none";
        removeDiv();
    }else{
        os_elem.style.display = "block";
        online_elem.style.display = "block";
        offline_elem.style.display = "none";
        removeDiv();;
    }
});
window.addEventListener('offline', ()=>{
    if (os_elem.style.display === "none") {
        os_elem.style.display = "block";
        online_elem.style.display = "none";
        offline_elem.style.display = "block";
        
        //removeDiv();
    }else{
        os_elem.style.display = "block";
        online_elem.style.display = "none";
        offline_elem.style.display = "block";
        //removeDiv();
    }
});

//import successJson from "@/json/data.json";

//import axios from 'axios';
//import axios from '@/plugins/axios';

export default {
    
    name: "MainBase",
    SendService,
    //defaultURL: process.env.BASE_URL+'server/api/controller/',
    //successJson,
    
    userType() {
        let u_t = this.getSessionItem('process_data');
        if (u_t === null || u_t === undefined || u_t.data == undefined) {
            return "Guest";
        }
        if (u_t && u_t.data.user.role == 'Guest') {
            return "Guest";
        }
        if (u_t && u_t.data.user.role == 'Admin') {
            return "Admin";
        }
        if (u_t && u_t.data.user.role == 'User') {
            return "User";
        }
        let urole = u_t.data.user.role;
        if (u_t && urole === null || urole == undefined || urole === undefined){
            return "Guest";
        }
    },
    updateStoreProcessDataString(string, data){
        if(string == "store"){
            const pdata = JSON.parse(localStorage.getItem('process_data'));
            if(pdata){
                if(pdata.hasOwnProperty('data')){
                    if(pdata.data.hasOwnProperty('user')){
                        if(pdata.data.user.hasOwnProperty('store')){
                            pdata.data.user.store = data;
                            localStorage.setItem("process_data", JSON.stringify(pdata));
                            return true;
                        }
                    }
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
    },
    updateProcessDataString(string, data){
        if(string){
            const pdata = JSON.parse(localStorage.getItem('process_data'));
            if(pdata){
                if(pdata.hasOwnProperty(string)){
                    pdata[string] = data;
                    localStorage.setItem("process_data", JSON.stringify(pdata));
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }
        
    },
    guestUser(){
        if(this.userType() == "Guest"){
            return true;
        }else{
            return false;
        }
    },

    adminLoggedIn(){
        if(this.userType() == 'Admin'){
            return true;
        }else{
            return false;
        }
    },

    userLoggedIn(){
        if(this.userType() == 'User'){
            return true;
        }else{
            return false;
        }
    },
    scrollTo(data){
        if(data == "top"){
            setTimeout(function(){
                window.scroll({
                    top: 0,
                    left: 0,
                    behavior: 'smooth'
                });
            }, 500);
        }
        if(data == "bottom"){
            window.scroll({
                top: 0,
                left: 0,
                behavior: 'smooth'
            });
        }
    },
    dismissAlert(){
        M.Toast.dismissAll();
    },

    alert(type, message){
        if(type == "success"){
            let msg = '<span class="ti ti-bell"></span>&nbsp;' + '<span style="font-size: 13px !important; font-weight: bold;">' + message + '</span>';
            M.toast({html: msg, displayLength: 3000, outDuration: 1000, classes:'toast_success'});
        }

        if(type == "error"){
            let msg = '<span class="ti ti-flag"></span>&nbsp;' + '<span style="font-size: 13px !important; font-weight: bold;">' + message + '</span>';
            M.toast({html: msg, displayLength: 5000, outDuration: 1000, classes:'toast_error'});
        }
        
        if(type == "info"){
            let msg = '<span class="ti ti-info"></span>&nbsp;' + '<span style="font-size: 13px !important; font-weight: bold;">' + message + '</span>';
            M.toast({html: msg, displayLength: 4000, outDuration: 1000, classes:'toast_info'});
        }

        if(type == "warning"){
            let msg = '<span class="ti ti-signal"></span>&nbsp;' + '<span style="font-size: 13px !important; font-weight: bold;">' + message + '</span>';
            M.toast({html: msg, displayLength: 4000, outDuration: 1000, classes:'toast_warning'});
        }

        if(type == "sessionerror"){
            alert(message);
        }
    },
    
    setSessionItem(key, value){
        localStorage.setItem(key, value);
    },

    getSessionItem(key){
        return JSON.parse(localStorage.getItem(key));
    },
    convertToBase64(oururl){
        let reader  = new FileReader();

        reader.addEventListener("load", function () {
            console.log(reader.result);
        }.bind(this), false);

        console.log(reader.readAsDataURL( oururl));
        
        
    },

    clearSession(){
        localStorage.clear();
    },
    logoutUser(){
        //alert("i am about to clear session");
        //localStorage.setItem("usertype", "guest");
        this.clearSession();
        return true;
        //localStorage.clear();
        //localStorage.setItem("usertype", "guest");
        //window.location.href = "";
        //this.push('login');
        //resolve();
    },

    closeModal(data){
        var elem = document.getElementById(data);
        var instance = M.Modal.getInstance(elem);
        instance.close();
    },

    openModal(data){
        var elem = document.getElementById(data);
        var instance = M.Modal.getInstance(elem);
        instance.open();
    },
    onlineStatus(){
        var os_elem = document.getElementById("onlinestatus");
        var online_elem = document.getElementById("onlinestat");
        var offline_elem = document.getElementById("offlinestat");

        if(window.navigator.onLine){
            return true;
        }else{
            if (os_elem.style.display === "none") {
                os_elem.style.display = "block";
                online_elem.style.display = "none";
                offline_elem.style.display = "block";
                //removeDiv();
            }else{
                os_elem.style.display = "block";
                online_elem.style.display = "none";
                offline_elem.style.display = "block";
                //removeDiv();
            }
            return false;
        }
    },

    showProgressBar(){
        var elem = document.getElementById("progressbar");
        if (elem.style.display === "none") {
            elem.style.display = "block";
        }else{
            elem.style.display = "block";
        }
    },

    removeProgressBar(){
        var elem = document.getElementById("progressbar");
        if (elem.style.display === "none") {
            elem.style.display = "none";
        } else {
            elem.style.display = "none";
        }
    },

    myConsole(string, data){
        console.log(string, data);
    },

    closeModal(data){
        var elem = document.getElementById(data);
        var instance = M.Modal.getInstance(elem);
        instance.close();
    },

    openModal(data){
        let elem = document.getElementById(data);
        let instance = M.Modal.getInstance(elem);
        instance.open();
    },

    nonDismissibleModal(){
        let elem= document.querySelectorAll('.modal');
        let instance = M.Modal.init(elem, {dismissible: false});
    },
    myConsole(string, data){
        console.log(string, data);
    },

    getGuestMenu(){
        let send_data = {
            string: "view_getmenus"
        }
        return this.SendService.get(send_data);
    },
    recentPosts(){
        let send_data = {
            string: "recentposts"
        }
        return this.SendService.get(send_data);
    },
    getTestimonies(){
        let send_data = {
            string: "gettestimonies"
        }
        return this.SendService.get(send_data);
    },
    getPartners(){
        let send_data = {
            string: "getpartners"
        }
        return this.SendService.get(send_data);
    },
    getPageData(send_data){
        send_data['string'] = "getpagedata/"+send_data['slug'];
        return this.SendService.get(send_data);
    },
    getCategoryData(send_data){
        send_data['string'] = "getcategorydata/"+send_data['slug'];
        return this.SendService.get(send_data);
    },
    getPostsData(send_data){
        send_data['string'] = "getpostsdata";
        return this.SendService.get(send_data);
    },
    getPostData(send_data){
        send_data['string'] = "getpostdata/"+send_data['slug'];
        return this.SendService.get(send_data);
    },
    //learnbread start
    getUserProfile(send_data){
        send_data['string'] = 'getuserprofile';
        return this.SendService.get(send_data);
    },
    getUserCategories(send_data){
        send_data['string'] = 'getusercategories';
        return this.SendService.get(send_data);
    },
    seeMoreGetUserCategories(send_data){
        send_data['string'] = 'seemoregetusercategories';
        return this.SendService.get(send_data);
    },
    subscribeToChannel(send_data){
        send_data['string'] = 'subscribetochannel';
        return this.SendService.post(send_data);
    },
    unsubscribeFromChannel(send_data){
        send_data['string'] = 'unsubscribefromchannel';
        return this.SendService.post(send_data);
    },
    updateChannel(send_data){
        send_data['string'] = 'updatechannel';
        return this.SendService.post(send_data);
    },
    getOurPrices(){
        let send_data = {
            string: "ourprices"
        }
        return this.SendService.post(send_data);
    },
    getSearchResult(send_data){
        send_data['string'] = 'getsearchresult';
        return this.SendService.get(send_data);
    },
    getUserChannels(send_data){
        send_data['string'] = 'getuserchannels';
        return this.SendService.get(send_data);
    },
    getUserStore(send_data){
        send_data['string'] = 'getuserstore';
        return this.SendService.get(send_data);
    },
    seeMoreGetUserChannels(send_data){
        send_data['string'] = 'seemoregetuserchannels';
        return this.SendService.get(send_data);
    },
    seeMoreGetUserStore(send_data){
        send_data['string'] = 'seemoregetuserstore';
        return this.SendService.get(send_data);
    },
    getSuggestedChannels(send_data){
        send_data['string'] = 'getsuggestedchannels';
        return this.SendService.get(send_data);
    },
    seeMoreGetSuggestedChannels(send_data){
        send_data['string'] = 'seemoregetsuggestedchannels';
        return this.SendService.get(send_data);
    },
    
    confirmUserSending(send_data){
        send_data['string'] = "confirmusersending";
        return this.SendService.get(send_data);
    },
    shareBread(send_data){
        send_data['string'] = "sharebread";
        return this.SendService.get(send_data);
    },
    getLearningZoneCatData(send_data){
        send_data['string'] = 'getlearningzonecatdata';
        return this.SendService.get(send_data);
    },
    readNextNote(send_data){
        send_data['string'] = 'readnextnote';
        return this.SendService.get(send_data);
    },
    viewNextScout(send_data){
        send_data['string'] = 'viewnextscout';
        return this.SendService.get(send_data);
    },
    getCategoryNotes(send_data){
        send_data['string'] = 'getcategorynotes';
        return this.SendService.get(send_data);
    },
    getMoreCategoryNotes(send_data){
        send_data['string'] = 'getmorecategorynotes';
        return this.SendService.get(send_data);
    },
    userLogin(d_data){
        d_data['string'] = 'userlogin';
        return this.SendService.post(d_data);
    },
    getSlug(str_data){
        let new_value = str_data.toLowerCase();
		return new_value.replace(/ /g, "-");
    },
    getCountries(){
        let d_data = {
            string: 'ourcountries'
        }
        return this.SendService.post(d_data);
    },
    getCategoryTypes(){
        let d_data = {
            string: 'ourcategorytypes'
        }
        return this.SendService.post(d_data);
    },
    createCategory(d_data){
        d_data['string'] = 'createcat';
        return this.SendService.post(d_data);
    },
    createNote(d_data){
        d_data['string'] = 'createnote';
        return this.SendService.post(d_data);
    },
    saveCategoryChanges(d_data){
        d_data['string'] = 'savecategorychanges';
        return this.SendService.post(d_data);
    },
    saveNoteChanges(d_data){
        d_data['string'] = 'savenotechanges';
        return this.SendService.post(d_data);
    },
    deleteUserCategory(d_data){
        d_data['string'] = 'deletecat';
        return this.SendService.post(d_data);
    },
    deleteUserNote(d_data){
        d_data['string'] = 'deletenote';
        return this.SendService.post(d_data);
    },
    createChannel(d_data){
        d_data['string'] = 'reguser';
        return this.SendService.post(d_data);
    },
    recoverChannel(d_data){
        d_data['string'] = 'resetpassword';
        return this.SendService.post(d_data);
    },
    createNewPassword(data){
        data['string'] = 'createnewpassword';
        return this.SendService.post(data);
    },
    
};