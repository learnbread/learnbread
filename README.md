# README #
Hello Team,
This is the README of this app and it would help for documentation of steps that are necessary to understand our app and get our application up and running.

# DESIGN ARCHITECTURE #
The design basically includes two (2) major sections, which are;
1. the Frontend
2. the  Backend

# THE FRONTEND #
The frontend section includes the designs which is developed with VueJs.

All the files here belongs to the frontend, except for the "backend" directory which  is specifically for the server-side.

# NOTE #
The design was done with MVC model in mind. Though it might not be a perfect MVC for now, but it is trying its very best to become one.

== Model == 

The database connection can be found at the following directory and file

* backend/config/dbConn.py

== View ==

Every thing about the view pages and frontend includes;

* Every file except the "backend" directory

== Controller ==

This part of the file helps communicate and get access to our methods, functions and , and helps control the flow of every HTTP request made to our controller and it is named "BaseController.py".
You can find this file in the director;

* backend/mainbase/BaseController.py


# USAGE AND FLOW OF APP #
Each of this frontend pages at some point make access to the API (which is the "backend" directory).

# For Example #
On visit to home page a HTTP request is made to/with the URL "localhost/backend/".

Where the last part of the URL is the route. By that i mean "/".

And to have access to any of the API endpoint, each request must go through a route, which is named "ourroute". And this can be found the the directory as thus;

* backend/ourroute.py

So at ourroute.py whenever there is a HTTP request with route "/", the function below the route is called.

# Note #
check the backend/ourroute.py file for more understanding


# The BACKEND #
So lets say the landing page for every HTTP request made to our server-side (backend, script, API endpoint) is "ourroute.py"

Note:
This is module based (also know as class based). So all our custom modules are saved the "classes" directory which can be seen;
* backend/classes

# IMPORTS #
For us to be able to use the method of other classes, the class instance is created at the top of every module. And that is done by using the import statement to import each module or class.

So at the top of our codes in the "ourroute.py" script, we imported the OurMiddleWare class and the BaseController's class which is named BaseClass.

# CALLING OF METHODS #
To call a particular method in a class instance, it is done as thus;

* classinstance.nameofmethod()

If that particular method is expecting parameters or arguments, then the parameters should be passed as expected from the method or function created.

# BASE CONTROLLER or BASE CLASS #
For us not to have repeated codes or functions the BaseController helps us navigate to a particular function or method in our SendServices Module.
The base controller is found;
* backend/mainbase/BaseController.py


# SENDSERVICES MODULE #
This is where all the functions and methods that carryout all the basic services or work we want are written. and the module can be found;
* backend/classes/sendservice.py

So the methods in the SendServices modules are in the following category;

C R U D     

or also know as 

I  S  U  D

where;
C = CREATE , R = READ, U = UPDATE, D = DELETE 

I = INSERT, S = SELECT, U = UPDATE, D = DELETE

So inside the SendService Module we have the following methods;
* insert():
* select():
* update():
* delete():

So every other methods aside the above mentioned are utility methods written to perform other activities.

# NOTE #
To navigate to a method or class, VSCode Editor has a feature that allows you to get to the destination of a class or method.

To do that;
* Right click on the method or class
* from the drop down list, select the "Go to Definition" option.